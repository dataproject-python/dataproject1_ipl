import csv
import matplotlib.pyplot as plt
import numpy as np

matchesDetailedDataPath = "/home/hp/Desktop/MOUNTBLUE/DATA_PROJECTS/PROJECT1/matches_detailed_data.csv"
umpiresDataFilePath = "/home/hp/Desktop/MOUNTBLUE/DATA_PROJECTS/PROJECT1/umpires.csv"
matchesFilePath = "/home/hp/Desktop/MOUNTBLUE/DATA_PROJECTS/PROJECT1/matches.csv"


# 1st CODE


def plotTotalRunsScoredByTeams(teamNames, totalRunsScoredByTeam):

    plt.figure(figsize=(20, 8))
    plt.barh(teamNames, totalRunsScoredByTeam)
    plt.xlabel("RUNS", fontsize=12)
    plt.ylabel("TEAMS", fontsize=12)
    plt.title("TOTAL RUNS SCORED BY TEAM", fontsize=15)
    plt.show()


def calculateTotalRunsScoredByTeams(matchesDetailedDataPath):

    file = open(matchesDetailedDataPath)
    csvreader = csv.DictReader(file)
    allTeamsRunsCounts = {}
    for row in csvreader:
        teamName = row['batting_team']
        runs = int(row['total_runs'])
        if teamName not in allTeamsRunsCounts:
            allTeamsRunsCounts[teamName] = runs
        else:
            allTeamsRunsCounts[teamName] += runs

    allTeamsRunsCounts = dict(
        sorted(allTeamsRunsCounts.items(), key=lambda x: x[1]))

    teamNames = list(allTeamsRunsCounts.keys())
    totalRunsScoredByTeam = list(allTeamsRunsCounts.values())

    return teamNames, totalRunsScoredByTeam


def executeTotalRunsScoredByTeams():

    teamNames, totalRunsScoredByTeam = calculateTotalRunsScoredByTeams(
        matchesDetailedDataPath)
    plotTotalRunsScoredByTeams(teamNames, totalRunsScoredByTeam)


# 2nd CODE


def plotTopBatsmansOfRCB(allRcbBatsmansNames, allRcbBatsmansRuns):

    plt.figure(figsize=(30, 60))
    # Creating a Horizontal Bar PLot using barh
    plt.barh(allRcbBatsmansNames, allRcbBatsmansRuns)
    plt.xlabel("RUNS", fontsize=12)
    plt.ylabel("BATSMANS", fontsize=12)
    plt.yticks(fontsize=6)
    plt.title("TOP SCORING BATSMAN'S FOR ROYAL CHALLENGERS BANGALORE", fontsize=15)

    plt.show()


def calculateTopBatsmansOfRCB(matchesDetailedDataPath):

    file = open(matchesDetailedDataPath)
    csvreader = csv.DictReader(file)

    allRCBBatsmansWithRuns = {}

    for row in csvreader:
        battingTeamName = row['batting_team']
        if battingTeamName == 'Royal Challengers Bangalore':
            batsmanName = row['batsman']
            batsmanRuns = int(row['batsman_runs'])
            if batsmanName not in allRCBBatsmansWithRuns:
                allRCBBatsmansWithRuns[batsmanName] = batsmanRuns
            else:
                allRCBBatsmansWithRuns[batsmanName] += batsmanRuns

    file.close()

    allRCBBatsmansWithRuns = dict(
        sorted(allRCBBatsmansWithRuns.items(), key=lambda x: x[1]))
    allRcbBatsmansNames = list(allRCBBatsmansWithRuns.keys())
    allRcbBatsmansRuns = list(allRCBBatsmansWithRuns.values())

    return allRcbBatsmansNames, allRcbBatsmansRuns


def executeTopBatsmansOfRCB():

    allRcbBatsmansNames, allRcbBatsmansRuns = calculateTopBatsmansOfRCB(
        matchesDetailedDataPath)
    plotTopBatsmansOfRCB(allRcbBatsmansNames, allRcbBatsmansRuns)


# 3rd CODE


def plotForeignUmpiresCounts(foreignCountriesNames, foreignCountriesCounts):

    plt.figure(figsize=(8, 8))
    plt.barh(foreignCountriesNames, foreignCountriesCounts)
    plt.xlabel("NUMBER OF UMPIRES", fontsize=12)
    plt.ylabel("COUNTRIES", fontsize=12)
    plt.title("FOREIGN UMPIRE ANALYSIS", fontsize=15)
    plt.show()


def calculateForeignUmpiresCounts(matchesFilePath, umpiresDataFilePath):

    file = open(umpiresDataFilePath)
    csvreader = csv.DictReader(file)
    umpireNamesWithCountries = {}
    for row in csvreader:
        umpireName = row['umpire']
        umpireCountry = row[' country'][1:]
        umpireNamesWithCountries[umpireName] = umpireCountry
    file.close()

    file = open(matchesFilePath)
    csvreader = csv.DictReader(file)

    allForeignUmpiresCountriesCounts = {}

    for row in csvreader:
        umpireName1 = row['umpire1']
        umpireName2 = row['umpire2']

        if umpireName1 == '' or umpireName2 == '':
            continue
        countryName = umpireNamesWithCountries[umpireName1]
        if countryName not in allForeignUmpiresCountriesCounts:
            allForeignUmpiresCountriesCounts[countryName] = 1
        else:
            allForeignUmpiresCountriesCounts[countryName] += 1

        countryName = umpireNamesWithCountries[umpireName2]
        if countryName not in allForeignUmpiresCountriesCounts:
            allForeignUmpiresCountriesCounts[countryName] = 1
        else:
            allForeignUmpiresCountriesCounts[countryName] += 1
    allForeignUmpiresCountriesCounts.pop('India')
    allForeignUmpiresCountriesCounts = dict(
        sorted(allForeignUmpiresCountriesCounts.items(), key=lambda x: x[1]))

    foreignCountriesNames = list(allForeignUmpiresCountriesCounts.keys())
    foreignCountriesCounts = list(allForeignUmpiresCountriesCounts.values())

    file.close()

    return foreignCountriesNames, foreignCountriesCounts


def executeForeignUmpiresCounts():

    foreignCountriesNames, foreignCountriesCounts = calculateForeignUmpiresCounts(
        matchesFilePath, umpiresDataFilePath)
    plotForeignUmpiresCounts(foreignCountriesNames, foreignCountriesCounts)


# 4th CODE


def plotMatchesPlayedByTeamBySeason(allTeamsNames, allCountsListsForEachYear):

    # Bars for creating Stacked Chart/Plot

    y1 = allCountsListsForEachYear[0]
    y2 = allCountsListsForEachYear[1]
    y3 = allCountsListsForEachYear[2]
    y4 = allCountsListsForEachYear[3]
    y5 = allCountsListsForEachYear[4]
    y6 = allCountsListsForEachYear[5]
    y7 = allCountsListsForEachYear[6]
    y8 = allCountsListsForEachYear[7]
    y9 = allCountsListsForEachYear[8]
    y10 = allCountsListsForEachYear[9]

    # Here we are creating horizontal Stacked bar for that we will use left attribute

    plt.figure(figsize=(15, 15))
    plt.barh(allTeamsNames, y1, label="2008")
    plt.barh(allTeamsNames, y2, left=y1, label="2009")
    plt.barh(allTeamsNames, y3, left=y1+y2, label="2010")
    plt.barh(allTeamsNames, y4, left=y1+y2+y3, label="2011")
    plt.barh(allTeamsNames, y5, left=y1+y2+y3+y4, label="2012")
    plt.barh(allTeamsNames, y6, left=y1+y2+y3+y4+y5, label="2013")
    plt.barh(allTeamsNames, y7, left=y1+y2+y3+y4+y5+y6, label="2014")
    plt.barh(allTeamsNames, y8, left=y1+y2+y3+y4+y5+y6+y7, label="2015")
    plt.barh(allTeamsNames, y9, left=y1+y2+y3+y4+y5+y6+y7+y8, label="2016")
    plt.barh(allTeamsNames, y10, left=y1+y2+y3+y4+y5+y6+y7+y8+y9, label="2017")
    plt.xlabel("NUMBER OF MATCHES PLAYED", fontsize=12)
    plt.ylabel("TEAMS", fontsize=12)
    plt.title("MATCHES PLAYED BY TEAM BY SEASON", fontsize=15)
    plt.legend()
    plt.show()


def calculateMatchesPlayedByTeamBySeason(matchesFilePath):

    file = open(matchesFilePath)
    csvreader = csv.DictReader(file)
    allTeamsNames = [
        "Sunrisers Hyderabad", "Royal Challengers Bangalore", "Mumbai Indians", "Rising Pune Supergiant",
        "Gujarat Lions", "Kolkata Knight Riders", "Kings XI Punjab",
        "Delhi Daredevils", "Chennai Super Kings", "Rajasthan Royals",
        "Deccan Chargers", "Kochi Tuskers Kerala", "Pune Warriors"
    ]
    allTeamsCountsYearwise = {}
    for yearIndex in range(2007, 2018):
        allTeamsCountsYearwise[yearIndex] = {}
        for team in allTeamsNames:
            allTeamsCountsYearwise[yearIndex][team] = 0
    count = 0
    y = []
    for row in csvreader:
        year = int(row['season'])
        team1 = row['team1']
        team2 = row['team2']
        if team1 == "Rising Pune Supergiant" or team2 == "Rising Pune Supergiant":
            if year not in y:
                y.append(year)
            count += 1

        allTeamsCountsYearwise[year][team1] += 1
        allTeamsCountsYearwise[year][team2] += 1
    allCountsListsForEachYear = []
    for year in allTeamsCountsYearwise:
        counts = list(allTeamsCountsYearwise[year].values())
        allCountsListsForEachYear.append(counts)

    allCountsListsForEachYear.pop(0)
    allCountsListsForEachYear = np.array(allCountsListsForEachYear)

    file.close()

    return allTeamsNames, allCountsListsForEachYear


def executeMatchesPlayedByTeamBySeason():

    allTeamsNames, allCountsListsForEachYear = calculateMatchesPlayedByTeamBySeason(
        matchesFilePath)
    plotMatchesPlayedByTeamBySeason(allTeamsNames, allCountsListsForEachYear)


# 5th CODE


def plotMatchesPlayedPerYearForAllYears(all_years, all_years_counts):

    plt.figure(figsize=(14, 14))
    plt.bar(all_years, all_years_counts)
    plt.title("NUMBER OF MATCHES PLAYED BY EACH TEAM", fontsize=20)
    plt.xlabel("YEARS", fontsize=15)
    plt.ylabel("NUMBER OF MATCHES PLAYED", fontsize=15)
    plt.xticks(all_years)
    plt.show()


def calculateMatchesPlayedPerYearForAllYears(matchesFilePath):

    file = open(matchesFilePath)
    csvreader = csv.DictReader(file)
    allYearsWithCounts = {}
    for row in csvreader:
        year = int(row['season'])
        if year not in allYearsWithCounts:
            allYearsWithCounts[year] = 0
        allYearsWithCounts[year] += 1
    allYearsWithCounts = dict(
        sorted(allYearsWithCounts.items(), key=lambda x: x[0]))
    all_years = list(allYearsWithCounts.keys())
    all_years_counts = list(allYearsWithCounts.values())

    file.close()

    return all_years, all_years_counts


def executetMatchesPlayedPerYearForAllYears():

    all_years, all_years_counts = calculateMatchesPlayedPerYearForAllYears(
        matchesFilePath)
    plotMatchesPlayedPerYearForAllYears(all_years, all_years_counts)


# 6th CODE


def plotNumberOfMatchesWonPerTeamPerYear(allTeamsNames, allTeamsWinningCountsPerYear):

    # Bars for creating Stacked Chart/Plot

    y1 = allTeamsWinningCountsPerYear[0]
    y2 = allTeamsWinningCountsPerYear[1]
    y3 = allTeamsWinningCountsPerYear[2]
    y4 = allTeamsWinningCountsPerYear[3]
    y5 = allTeamsWinningCountsPerYear[4]
    y6 = allTeamsWinningCountsPerYear[5]
    y7 = allTeamsWinningCountsPerYear[6]
    y8 = allTeamsWinningCountsPerYear[7]
    y9 = allTeamsWinningCountsPerYear[8]
    y10 = allTeamsWinningCountsPerYear[9]

    # Here we are creating horizontal Stacked bar for that we will use left attribute

    plt.figure(figsize=(15, 15))
    plt.barh(allTeamsNames, y1, label="2008")
    plt.barh(allTeamsNames, y2, left=y1, label="2009")
    plt.barh(allTeamsNames, y3, left=y1+y2, label="2010")
    plt.barh(allTeamsNames, y4, left=y1+y2+y3, label="2011")
    plt.barh(allTeamsNames, y5, left=y1+y2+y3+y4, label="2012")
    plt.barh(allTeamsNames, y6, left=y1+y2+y3+y4+y5, label="2013")
    plt.barh(allTeamsNames, y7, left=y1+y2+y3+y4+y5+y6, label="2014")
    plt.barh(allTeamsNames, y8, left=y1+y2+y3+y4+y5+y6+y7, label="2015")
    plt.barh(allTeamsNames, y9, left=y1+y2+y3+y4+y5+y6+y7+y8, label="2016")
    plt.barh(allTeamsNames, y10, left=y1+y2+y3+y4+y5+y6+y7+y8+y9, label="2017")
    plt.xlabel("NUMBER OF MATCHES WON", fontsize=12)
    plt.ylabel("TEAMS", fontsize=12)
    plt.title("MATCHES WON BY TEAM BY SEASON", fontsize=15)
    plt.legend()
    plt.show()


def calculateNumberOfMatchesWonPerTeamPerYear(matchesFilePath):

    file = open(matchesFilePath)
    csvreader = csv.DictReader(file)
    allTeamsNames = [
        "Sunrisers Hyderabad", "Royal Challengers Bangalore", "Mumbai Indians", "Rising Pune Supergiant",
        "Gujarat Lions", "Kolkata Knight Riders", "Kings XI Punjab",
        "Delhi Daredevils", "Chennai Super Kings", "Rajasthan Royals",
        "Deccan Chargers", "Kochi Tuskers Kerala", "Pune Warriors"
    ]
    allTeamsYearwiseWinningCount = {}
    for yearIndex in range(2007, 2018):
        allTeamsYearwiseWinningCount[yearIndex] = {}
        for team in allTeamsNames:
            allTeamsYearwiseWinningCount[yearIndex][team] = 0
    for row in csvreader:
        try:
            year = int(row['season'])
            winnerTeam = row['winner']
            allTeamsYearwiseWinningCount[year][winnerTeam] += 1
        except:
            print('WINNER TEAM NOT AVAILABLE')

    allTeamsWinningCountsPerYear = []
    for year in allTeamsYearwiseWinningCount:
        countPerYear = allTeamsYearwiseWinningCount[year].values()
        allTeamsWinningCountsPerYear.append(list(countPerYear))

    allTeamsWinningCountsPerYear.pop(0)
    allTeamsWinningCountsPerYear = np.array(allTeamsWinningCountsPerYear)

    file.close()

    return allTeamsNames, allTeamsWinningCountsPerYear


def executeNumberOfMatchesWonPerTeamPerYear():

    allTeamsNames, allTeamsWinningCountsPerYear = calculateNumberOfMatchesWonPerTeamPerYear(
        matchesFilePath)
    plotNumberOfMatchesWonPerTeamPerYear(
        allTeamsNames, allTeamsWinningCountsPerYear)


# 7th Extra runs conceded per team in the year 2016


def plotExtraRunsConconcededPerTeamInYear2016(allTeamsNames, allTeamsExtras):

    plt.figure(figsize=(12, 12))
    plt.barh(allTeamsNames, allTeamsExtras)
    plt.title('ALL TEAMS EXTRA RUNS IN YEAR 2016', fontsize=20)
    plt.xlabel('RUNS', fontsize=15)
    plt.ylabel('TEAMS', fontsize=15)
    plt.show()


def calculateExtraRunsConconcededPerTeamInYear2016(matchesFilePath, matchesDetailedDataPath):

    file1 = open(matchesFilePath)
    file2 = open(matchesDetailedDataPath)
    dictReader1 = csv.DictReader(file1)
    dictReader2 = csv.DictReader(file2)
    matchesIds = []
    for row in dictReader1:
        year = int(row['season'])
        if year == 2016:
            matchId = int(row['id'])
            matchesIds.append(matchId)
    allTeamsExtraRuns = {}
    for row in dictReader2:
        matchId = int(row['match_id'])
        if matchId in matchesIds:
            bowlingTeam = row['bowling_team']
            extraRuns = int(row['extra_runs'])
            if bowlingTeam not in allTeamsExtraRuns:
                allTeamsExtraRuns[bowlingTeam] = 0
            allTeamsExtraRuns[bowlingTeam] += extraRuns

    allTeamsExtraRuns = dict(
        sorted(allTeamsExtraRuns.items(), key=lambda x: x[1]))

    allTeamsNames = list(allTeamsExtraRuns.keys())
    allTeamsExtras = list(allTeamsExtraRuns.values())

    file1.close()
    file2.close()

    return allTeamsNames, allTeamsExtras


def executeExtraRunsConconcededPerTeamInYear2016():

    allTeamsNames, allTeamsExtras = calculateExtraRunsConconcededPerTeamInYear2016(
        matchesFilePath, matchesDetailedDataPath)
    plotExtraRunsConconcededPerTeamInYear2016(allTeamsNames, allTeamsExtras)


# 8th Top 10 economical bowlers in the year 2015


def plotTopTenEconomicalBowlersIn2015(topTenBowlersNames, topTenEconomies):

    plt.figure(figsize=(20, 25))
    plt.bar(topTenBowlersNames, topTenEconomies)
    plt.title("TOP 10 ECONOMICAL BOWLERS IN YEAR 2015", fontsize=25)
    plt.xlabel("BOWLER NAME", fontsize=20)
    plt.ylabel("ECONOMY OF BOWLER", fontsize=20)
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=13)
    plt.yticks(topTenEconomies)
    plt.show()


def calculateTopTenEconomicalBowlersIn2015(matchesFilePath, matchesDetailedDataPath):

    file1 = open(matchesFilePath)
    file2 = open(matchesDetailedDataPath)
    dictReader1 = csv.DictReader(file1)
    dictReader2 = csv.DictReader(file2)
    matchesIds = {}
    for row in dictReader1:
        year = int(row['season'])
        if year == 2015:
            matchId = int(row['id'])
            matchesIds[matchId] = 0
    allBowlersData = {}
    for row in dictReader2:
        matchId = int(row['match_id'])
        if matchId in matchesIds:
            bowler = row['bowler']
            if bowler not in allBowlersData:
                allBowlersData[bowler] = [0, 0, 0]
            extraRuns = int(row['total_runs'])
            extraRunsGivenIndex = 0
            totalBallsByBowlersIndex = 1
            economyIndex = 2
            allBowlersData[bowler][extraRunsGivenIndex] += extraRuns
            allBowlersData[bowler][totalBallsByBowlersIndex] += 1
            economy = (allBowlersData[bowler][extraRunsGivenIndex] /
                       allBowlersData[bowler][totalBallsByBowlersIndex])
            economy = round(economy * 6, 2)
            allBowlersData[bowler][economyIndex] = economy
    allBowlersData = dict(
        sorted(allBowlersData.items(), key=lambda x: x[1][2]))

    topTenBowlersNames = list(allBowlersData)[:10]
    topTenEconomies = []
    for index in range(10):
        topTenEconomies.append(list(allBowlersData.values())[index][2])

    file1.close()
    file2.close()

    return topTenBowlersNames, topTenEconomies


def executeTopTenEconomicalBowlersIn2015():

    topTenBowlersNames, topTenEconomies = calculateTopTenEconomicalBowlersIn2015(
        matchesFilePath, matchesDetailedDataPath)
    plotTopTenEconomicalBowlersIn2015(topTenBowlersNames, topTenEconomies)


if __name__ == '__main__':

    executeTotalRunsScoredByTeams()
    executeTopBatsmansOfRCB()
    executeForeignUmpiresCounts()
    executeMatchesPlayedByTeamBySeason()
    executetMatchesPlayedPerYearForAllYears()
    executeNumberOfMatchesWonPerTeamPerYear()
    executeExtraRunsConconcededPerTeamInYear2016()
    executeTopTenEconomicalBowlersIn2015()
